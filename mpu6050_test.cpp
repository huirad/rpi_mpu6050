/**************************************************************************
 * @brief Test program for mpu6050.cpp
 *
 * @copyright Copyright (C) 2015, Helmut Schmidt
 *
 * @license MPL-2.0 <http://spdx.org/licenses/MPL-2.0>
 *
 **************************************************************************/


#include "mpu6050.h"
#include <stdio.h>
#include <unistd.h>

void mpu6050_cb(const TMPU6050Vector3D acceleration[], const TMPU6050Vector3D gyro_angular_rate[], const float temperature[], const uint64_t timestamp[], const uint16_t num_elements)
{
    for (uint16_t i=0; i<num_elements; i++)
    {
        printf("%llu %hu A %+9.6f %+9.6f %+9.6f G %+7.4f %+7.4f %+7.4f T %+5.2f\n",
            timestamp[i],
            i,
            acceleration[i].x,
            acceleration[i].y,
            acceleration[i].z,
            gyro_angular_rate[i].x,
            gyro_angular_rate[i].y,
            gyro_angular_rate[i].z,
            temperature[i]
            );
    }
}


int main()
{
    bool result = false;
    TMPU6050Vector3D acceleration;
    TMPU6050Vector3D gyro_angular_rate;
    float temperature;
    uint64_t timestamp;
    uint64_t time_start;
    uint64_t time_end;
    int n_loops = 1000;

    result = mpu6050_init(MPU6050_I2C_DEV_1, MPU6050_ADDR_1, MPU6050_DLPF_98HZ);
    if (result)
    {
        result = true;
        time_start = mpu6050_get_timestamp();
        for (int i=0; i<n_loops; i++)
        {
            result = result && mpu6050_read_accel_gyro(&acceleration, &gyro_angular_rate, &temperature, &timestamp);  
        }
        time_end = mpu6050_get_timestamp();
        //printf ("%d Loops: Start %llu End %llu\n", n_loops, time_start, time_end);
/*        if (result)
        {
            printf("Acceleration: [%+7.5f, %+7.5f, %+7.5f] Angular Rate: [%+7.3f, %+7.3f, %+7.3f], Temperature: %+5.2f, Timestamp: %llu, Duration: %llu\n",
                acceleration.x,
                acceleration.y,
                acceleration.z,
                gyro_angular_rate.x,
                gyro_angular_rate.y,
                gyro_angular_rate.z,
                temperature,
                timestamp,
                time_end-time_start
                );
        }
        else
        {
            printf("ERROR: Could not read MPU6050\n");
        }
*/
      //Callback Test
        mpu6050_register_callback(&mpu6050_cb);
        mpu6050_start_reader_thread(2, 10, false);
        sleep(10);
        mpu6050_stop_reader_thread();
        mpu6050_deregister_callback(&mpu6050_cb);
    }
    else
    {
        printf("ERROR: Could not init MPU6050\n");
    }

    return 0;
}
