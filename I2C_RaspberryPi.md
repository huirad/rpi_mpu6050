I2C Setup on the Raspberry Pi
=============================
References: 
[1](http://www.netzmafia.de/skripten/hardware/RasPi/RasPi_I2C.html)
[2](http://blog.bitify.co.uk/2013/11/interfacing-raspberry-pi-and-mpu-6050.html)
[3](https://learn.adafruit.com/adafruits-raspberry-pi-lesson-4-gpio-setup/configuring-i2c)
[4](https://www.abelectronics.co.uk/i2c-raspbian-wheezy/info.aspx)
[5a](http://raspberry.tips/faq/raspberry-pi-spi-und-i2c-aktivieren/)
[5b](http://raspberry.tips/faq/raspberry-pi-device-tree-aenderung-mit-kernel-3-18-x-geraete-wieder-aktivieren/)

Install tools
------------------
* `sudo apt-get update`
* `sudo apt-get install i2c-tools`     # I2C-Toolkit for the command line
* `sudo apt-get install python-smbus`  # I2C python module
* `sudo apt-get install libi2c-dev`    # I2C library for C programming


Activate the I2C driver for kernels without devicetree
------------------------------------------------------
This is for old Raspian releases before January 2015. 
E.g. for 2014-09-09-wheezy-raspbian which I am using for my initial setup.

* `sudo nano /etc/modprobe.d/raspi-blacklist.conf`
    * \# blacklist i2c-bcm2708
* `sudo nano /etc/modules`
    * i2c-dev
* `sudo modprobe i2c-bcm2708`
* `sudo modprobe i2c-dev`

Activate the I2C driver for kernels _with_ devicetree
-----------------------------------------------------
Early 2015, with Kernel 3.18, the kernel configuration has changed 
to use the device tree. 
See 
[announcement](https://www.raspberrypi.org/forums/viewtopic.php?p=675658#p675658),
[device-tree.md](https://www.raspberrypi.org/documentation/configuration/device-tree.md),
[raspi-config.md](https://www.raspberrypi.org/documentation/configuration/raspi-config.md).
Note: you can check the kernel version by calling `uname -a`.

The following description is based on experience with 2015-05-05-raspbian-wheezy
and follows basically the tutorial from 
[adafruit](https://learn.adafruit.com/adafruits-raspberry-pi-lesson-4-gpio-setup/configuring-i2c).

* Activate the i2c-bcm2708 driver from Advanced Options menu of the `sudo raspi-config` command.
Note: You don't have to remove it from the blacklist in a fresh installation as the blacklist should be anyway empty.
* Add `i2c-dev` to `/etc/modules` as decribed above!
Note: [i2c-dev](https://www.kernel.org/doc/Documentation/i2c/dev-interface) 
is the driver providing the user space access to I2C.



Raspberry Pi hardware revisions and I2C bus adresses
----------------------------------------------------
I have one of the earliest Raspberry Pi's: model B, PCB Revision 1.
There the bus number is 0, i.e. the device address is /dev/i2c-0.
For most later models (starting already Q3/2012), the bus number is 1.
See the following links concerning the hardware variants and different I2C bus addresses: 
[1], [2], [3], [4], [5]

To find out, which device you have, either

* Call `cat /proc/cpuinfo` and look up in [3] or [4]. If version is >=4, then you have a new model.
* Look whether the PCB has mounting holes: If yes, then you have a new model.

[1]:http://www.themagpi.com/issue/issue-13/article/introduction-to-pi-matrix/
[2]:http://www.netzmafia.de/skripten/hardware/RasPi/RasPi_I2C.html
[3]:http://elinux.org/RPi_HardwareHistory
[4]:http://www.raspberrypi.org/upcoming-board-revision/
[5]:http://www.golem.de/news/raspberry-pi-revision-2-behebt-kleine-fehler-1209-94387.html

Test I2C access
---------------
* `lsmod | grep i2c`        #Check which I2C drivers are loaded
* `ls /dev | grep i2c`      #Check which I2C buses are accessible in user space
* `sudo i2cdetect -l`       #Check which I2C buses are accessible in user space
* `sudo i2cdetect -y 0` or for newer HW revisions: `sudo i2cdetect -y 1`

Allow I2C access without root rights
------------------------------------
* Check whether user pi is already member of group i2c 
    * `groups pi`
* With 2015-05-05-raspbian-wheezy on my Pi 2 this is the case
* With 2014-09-09-wheezy-raspbian on my old Pi 1 this is not the case
* If not: add user pi to group i2c 
    * `sudo adduser pi i2c`
    * `sudo reboot`


Connect the MPU9150
===================
* [Connect I2C](http://blog.bitify.co.uk/2013/11/interfacing-raspberry-pi-and-mpu-6050.html): 
  VCC to 3V3, GND to GND, SDA to SDA, SCL to SCL
    * Additional links with the RPi pin layout: 
    [1](http://raspberrypi.znix.com/hipidocs/topic_gpiopins.htm)
    [2](http://de.mathworks.com/help/supportpkg/raspberrypiio/ug/use-the-i2c-interface-on-the-raspberry-pi-hardware.html)
* Test MPU9150 presence: `i2cdetect -y 0 `
    * MPU9150 is detected with adress 68 (hex). The I2C address can be changed to 0x69. 
    See the [datasheet/register map](http://invensense.com/mems/gyro/documents/RM-MPU-6000A-00v4.2.pdf).
* Read data from MPU9150: `i2cget -y 0 0x68 0x75`
* Test access via [python script](http://blog.bitify.co.uk/2013/11/reading-data-from-mpu-6050-on-raspberry.html): OK
* Test access via C-program: 
    * Strange - I had to re-install libi2c-dev `sudo apt-get install libi2c-dev` to avoid linker errors

The following pictures show how I mounted a GY-521 module carrying a MPU6050 on a 
[Pibow Coupe](http://shop.pimoroni.com/products/pibow-coupe) case with a Pi 2
![GY521_Pi2_PibowCoupe_1][GY521_Pi2_PibowCoupe_1]
[GY521_Pi2_PibowCoupe_1]: GY521_Pi2_PibowCoupe_1.jpg
![GY521_Pi2_PibowCoupe_2][GY521_Pi2_PibowCoupe_2]
[GY521_Pi2_PibowCoupe_2]: GY521_Pi2_PibowCoupe_2.jpg

Measurement observations
========================

I2C performance
---------------
I2C reading out acceleration, angular rate, temperature requires reading 14 bytes net.
But there is 3 bytes overhead (sending SAD+W, SUB, sending SAD+R), so effectively 17 bytes are transmitted.
With a 100kHz I2C, a transfer time of 1.7ms would this be expected

Measurements:

* On my old P1, PCB Revision 1 with 2014-09-09-wheezy-raspbian, the 17byte transfer takes 5.28ms
    * This is 3 times longer as expected
* On my new P2, with 2015-05-05-raspbian-wheezy, the 17byte transfer takes 1.63ms
    * This is more or less as expected


Accelerometer offsets
---------------------
According the [data sheet](http://www.invensense.com/products/motion-tracking/6-axis/mpu-6050/)
the ZERO-G output is 

* for the X and Y axes: +- 50mg at 25°C with +-35mg tempeature change from 0°C to 70°C
* for the Z axis: +- 80mg at 25°C with +-60mg tempeature change from 0°C to 70°C

I have two GY521 boards carrying a MPU6050:

* The first board reads 950mg gravity (Z-axis accceleration) at 29°C on a flat surface
    * This is within the specification
* The second board reads 1170mg gravity (Z-axis accceleration) at 35°C on a flat surface
    * This appears to be out of the specification

The temperatures are so high, because testing was done on a hot summer day. 
On the second board, the MPU6050 apparently gets some heat from the Pi2 CPU.

Accelerometer offset correction - research
------------------------------------------
Some internet resarch shows that the MPU6050 can compensate accelerometer offset internally.
For this, the offsets have to be written into some registers which are not documented in the official register map.


* Some links refering to the need of calibration
[1](http://www.i2cdevlib.com/forums/topic/75-mpu-6050-accelerometer-calibration-and-dmp/)
[2](http://epics.ecn.purdue.edu/halp/img/DMP_Calibration_Instructions-2.pdf) 
[3](http://www.reddit.com/r/arduino/comments/2lxpl9/help_with_offset_calibration_on_my_mpu6050/) 
[4](http://hobbylogs.me.pn/?p=47) 
[5](http://wired.chillibasket.com/2015/01/self-balancing-robot-part-3/)

* I2CDevLib
[register map](http://www.i2cdevlib.com/devices/mpu6050#registers)
[MPU6050.h](https://github.com/jrowberg/i2cdevlib/blob/master/Arduino/MPU6050/MPU6050.h) - search for MPU6050_RA_ZA_OFFS_H
[MPU6050.cpp](https://github.com/jrowberg/i2cdevlib/blob/master/Arduino/MPU6050/MPU6050.cpp) - search for getZAccelOffset() 
Usage examples: 
[1](https://github.com/jrowberg/i2cdevlib/blob/master/Arduino/MPU6050/Examples/MPU6050_DMP6/MPU6050_DMP6.ino)
[2](https://github.com/jrowberg/i2cdevlib/blob/master/Arduino/MPU6050/Examples/MPU6050_raw/MPU6050_raw.ino)
* The orginal post of the calibration sketch
[1](http://www.i2cdevlib.com/forums/topic/96-arduino-sketch-to-automatically-calculate-mpu6050-offsets/) 
and a scientific paper fom the same author
[2](http://wks.gii.upv.es/cobami/files/ICUAS-2014.pdf)


* kriswiner has made a similar implementation 
[1](https://github.com/kriswiner/MPU-6050/blob/master/MPU6050IMU.ino) 
with better documentation (see below) but maybe still with an implementation bug 
[2](https://github.com/kriswiner/MPU-6050/issues/8)

~~~~
#define XA_OFFSET_H      0x06 // User-defined trim values for accelerometer
#define XA_OFFSET_L_TC   0x07
#define YA_OFFSET_H      0x08
#define YA_OFFSET_L_TC   0x09
#define ZA_OFFSET_H      0x0A
#define ZA_OFFSET_L_TC   0x0B
...
// Construct the accelerometer biases for push to the hardware accelerometer bias registers. These registers contain
// factory trim values which must be added to the calculated accelerometer biases; on boot up these registers will hold
// non-zero values. In addition, bit 0 of the lower byte must be preserved since it is used for temperature
// compensation calculations. Accelerometer bias registers expect bias input as 2048 LSB per g, so that
// the accelerometer biases calculated above must be divided by 8.
~~~~

Accelerometer offset correction - results
-----------------------------------------
Factory settings

* On my first GY521 board (the better one), the Z accelerometer offsets have been preprogrammed as 
    * `i2cget -y 0 0x68 0x0A` ==> 0x03
    * `i2cget -y 0 0x68 0x0B` ==> 0xea
* On my second GY521 board (the worse one), the Z accelerometer offsets have been preprogrammed as 
    * `i2cget -y 1 0x68 0x0A` ==> 0x07
    * `i2cget -y 1 0x68 0x0B` ==> 0xa2

This means that according to [1](https://github.com/kriswiner/MPU-6050/blob/master/MPU6050IMU.ino) 
already cited above, the temperature compensation bit is not set for either sample.

Coarse correction of the z-axis acceleration offset of the worse board:

* Set the offset registers to zero
    * `i2cset -y 1 0x68 0x0A 0x00`
    * `i2cset -y 1 0x68 0x0B 0x00`
* Measure the z-axis acceleration
    * `i2cget -y 1 0x68 0x3F w` ==> 0x580b and note that the bytes are reversed when i2cget reads a word
* Calculate the offset from the expected value (1g = 0x4000 = 16384)
    * (0x4000-0x0b58)/8 = 0x695
    * Clear the lowest bit as it has not been set before ==> 0x694
* Set the offset registers to the new values
    * `i2cset -y 1 0x68 0x0A 0x06`
    * `i2cset -y 1 0x68 0x0B 0x94`
* Now we reads 1030mg gravity (Z-axis accceleration) at 35°C on a flat surface
    * So now we are in the specification range
* Powering off the device and on again
    * The offset values which we have stored are gone and the original offset is active again (0x07/0xa2)
    
Further research, see also 
[my discussion with kriswiner](https://github.com/kriswiner/MPU-6050/issues/8)

* Get an Invense developer account at [http://www.invensense.com/developers](http://www.invensense.com/developers)
* Go to http://www.invensense.com/developers/software-downloads/ and download "Embedded MotionDriver v5.1.3"
    * Extract the document "MPU HW Offset Registers 1.2.pdf"
    * Find there the documentation of the HW Offset Registers and also the note that on startup the factory settings are restored
* Browse the forum and find some discussions about accelerometer z-axis offset indicating that the behaviour of my second board is not completely uncommon.
    * [1](http://www.invensense.com/forum/viewtopic.php?f=3&t=916&p=3457&hilit=offset#p3457)
       "This is due to the offset of our chip. For the 9150 it's 150mg on the z axis. Based on the setting and 8g full scale range, 150 is corresponding to LSB count 614."
    * [2](http://www.invensense.com/forum/viewtopic.php?f=3&t=927&p=3415&hilit=offset#p3415)
       "As you said once the power is reset, all the values should be restored to the default values. Register 0x0A is not configurable."    
    * [3](http://www.invensense.com/forum/viewtopic.php?f=3&t=262&p=1202&hilit=offset#p1202)
      "I experience an offset on the Z-axis of the accelerometer. It's always around 0.2 - 0.3 G."
      "... however the Z-Axis accelerometer has been known to shift offset due to package stress during the assembly process. 
       ... Typically, this shift is permanent and sensitivities are unaffected, so the functionality can be easily calibrated and compensated for in software."
 
  
Other potentially interesting links
===================================
* I2C access with perl: 
[1](http://raspberry.znix.com/)
[2](http://raspberrypi.znix.com/hipidocs/topic_i2cdev.htm)
* [GPIO benchmark](http://codeandlife.com/2012/07/03/benchmarking-raspberry-pi-gpio-speed/)
* Switching I2C from 100kHz to 400kHz bus speed
[1](http://www.allthingsmicro.com/tutorials/raspberrypi-enabling-i2c-bus)
[2](http://arduino-pi.blogspot.de/2014/03/speeding-up-i2c-bus-on-raspberry-pi-and.html)
[3](https://www.raspberrypi.org/forums/viewtopic.php?f=44&t=34734) Check clock speed `sudo cat /sys/module/i2c_bcm2708/parameters/baudrate`
 With device tree: [4](https://richardstechnotes.wordpress.com/2015/06/15/changing-the-raspberry-pipi2-i2c-bus-speed-device-tree-style/)
* BCM2835 clock stretching bug 
[1](http://www.advamation.de/technik/raspberrypi/rpi-i2c-bug.html)
[2](http://www.hobbytronics.co.uk/raspberry-pi-i2c-clock-stretching)
Also on Pi2: [3](http://www.mikrocontroller.net/topic/360817)