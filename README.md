RPi_MPU6050
===========
This C library encapsulates the access to the 
[MPU6050](http://www.invensense.com/mems/gyro/mpu6050.html) and 
[MPU9150](http://www.invensense.com/mems/gyro/mpu9150.html) sensors
via I2C.
It is primarily designed for use on the Raspberry Pi but should work 
on any Linux system.

Only the most basic functionality is implemented: waking up, shutting down, 
reading gyroscope and accelerometer data.
If you are looking for a more complete implementation, have a look at
the [i2cdevlib](https://github.com/jrowberg/i2cdevlib) library for which 
there exists already a 
[Raspberry Pi port](https://github.com/richardghirst/PiBits/tree/master/MPU6050-Pi-Demo)


TODO
----
* Improve error handling, e.g. introduce MPU6050ErrNo similar to http://linux.die.net/man/3/errno
* GENIVI Positioning PoC Integration: sns-use-mpu6950.c
* Optional: Allow to select sampling rate (probably not necessary when FIFO is not used)


DONE
----
* CHECK: I2C Performance RPi / CPU load
    * 5ms with Acc, Gyro [14 Bytes], 3ms with Acc [8 Bytes]

